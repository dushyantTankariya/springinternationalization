# Output #

### To check the output, start from the beneath URL and we will get the page as ###
 * http://localhost:8080/concretepage/user 
 
  ![concretepage](https://www.concretepage.com/spring-4/images/spring-4-mvc-internationalization-i18n-and-localization-l10n-annotation-example-2.jpg)
  
  
 * The default language is English. Now submit the form, we will get the page as given below.
 
  ![default-en](https://www.concretepage.com/spring-4/images/spring-4-mvc-internationalization-i18n-and-localization-l10n-annotation-example-3.jpg)
 
### What we need to observer that the language of website is English in all pages. Now change the website in German language by clicking the German link. Internally it calls ###
 * http://localhost:8080/concretepage/user?mylocale=de
 
  ![lang-de](https://www.concretepage.com/spring-4/images/spring-4-mvc-internationalization-i18n-and-localization-l10n-annotation-example-4.jpg)
  
  
 * Submit it then we will get the page as given below.

### Again we need to observe that now onwards all pages are in German. To change it again in English, click the English link that will internally call the URL ###
 * http://localhost:8080/concretepage/user?mylocale=en
 * As we are using `CookieLocaleResolver`, so what happens that it sets a cookie named as myLocaleCookie. Check it in the browser.